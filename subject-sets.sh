# TODO: This is not automated as of yet. This is just a copy

json_array=(
    ### ROLE:               AUTOBRANCH_X
    #########################################
    ### gemeente 's-Hertogenbosch (0796)
    ### INCLUDES DATA:      [KVK data 0796, ]
    '{"namespace":"permissions","object":"kvk_gm0796","relation":"access", "subject_set": {"namespace": "groups", "object": "autobranche_gm0796", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0796","relation":"issuer", "subject_set": {"namespace": "groups", "object": "autobranche_gm0796", "relation": "issuer"}}'

    ### gemeente Son en Breugel (0848)
    ### INCLUDES DATA:      [KVK data 0848, ]
    '{"namespace":"permissions","object":"kvk_gm0848","relation":"access", "subject_set": {"namespace": "groups", "object": "autobranche_gm0848", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0848","relation":"issuer", "subject_set": {"namespace": "groups", "object": "autobranche_gm0848", "relation": "issuer"}}'

    ### gemeente Pijnacker-Nootdorp (1926)
    ### INCLUDES DATA:      [KVK data 1926, ]
    '{"namespace":"permissions","object":"kvk_gm1926","relation":"access", "subject_set": {"namespace": "groups", "object": "autobranche_gm1926", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm1926","relation":"issuer", "subject_set": {"namespace": "groups", "object": "autobranche_gm1926", "relation": "issuer"}}'

    ### gemeente Veldhoven (0861)
    ### INCLUDES DATA:      [KVK data 0861, ]
    '{"namespace":"permissions","object":"kvk_gm0861","relation":"access", "subject_set": {"namespace": "groups", "object": "autobranche_gm0861", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0861","relation":"issuer", "subject_set": {"namespace": "groups", "object": "autobranche_gm0861", "relation": "issuer"}}'


    ### ROLE:               BEDRIJVENTERREINEN_X
    #################################################
    ### gemeente 's-Hertogenbosch (0796)
    ### INCLUDES DATA:      [KVK data 0796, ]
    '{"namespace":"permissions","object":"kvk_gm0796","relation":"access", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0796", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0796","relation":"issuer", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0796", "relation": "issuer"}}'

    ### gemeente Son en Breugel (0848)
    ### INCLUDES DATA:      [KVK data 0848, ]
    '{"namespace":"permissions","object":"kvk_gm0848","relation":"access", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0848", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0848","relation":"issuer", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0848", "relation": "issuer"}}'

    ### gemeente Pijnacker-Nootdorp (1926)
    ### INCLUDES DATA:      [KVK data 1926, ]
    '{"namespace":"permissions","object":"kvk_gm1926","relation":"access", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm1926", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm1926","relation":"issuer", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm1926", "relation": "issuer"}}'

    ### gemeente Veldhoven (0861)
    ### INCLUDES DATA:      [KVK data 0861, ]
    '{"namespace":"permissions","object":"kvk_gm0861","relation":"access", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0861", "relation": "access"}}'
    '{"namespace":"permissions","object":"kvk_gm0861","relation":"issuer", "subject_set": {"namespace": "groups", "object": "bedrijventerreinen_gm0861", "relation": "issuer"}}'


    ### ROLE:               GENERAL_X
    ####################################
    ### INCLUDES DATA:      [general_dego_data, ]
    '{"namespace":"permissions","object":"general_dego_data","relation":"access", "subject_set": {"namespace": "groups", "object": "general_dego", "relation": "access"}}'
    '{"namespace":"permissions","object":"general_dego_data","relation":"issuer", "subject_set": {"namespace": "groups", "object": "general_dego", "relation": "issuer"}}'
)
