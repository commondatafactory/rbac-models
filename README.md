# authorization-model

Each organization has roles and roles are related to data permissions.
So a member of an organization can have some roles and with it some data permissions.

Organization -> Roles -> Data permssions.

A user can be member of organizaions and roles withing organizations.
and a active must select and activate the current active organization and role currently
working on. See  auth-service https://gitlab.com/commondatafactory/cdf-identity-platform/auth-service

The model is shared with auth-api, ory and other services as shared knowledge and gets translated into
ory keto - tuples.

usage: simply GET the url of the model. there acc production, devloper and dev versions

For example:

`generate-authorizations.sh`
```sh
#!/bin/bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

url="https://gitlab.com/commondatafactory/rbac-models/-/raw/main/authorization-model.json"
output_file="authorization-model.json"

curl -o "$output_file" "$url"
```


Currently we have access, issuer and admin relations.

- access , currently active
- issuer , can make role or organization active
- admin  , can make other users issuer of role / organizaion
